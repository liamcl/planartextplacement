﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadVertexRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotation;

    Mesh mesh;
    Vector3[] vertices;
    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
        Vector3 center = mesh.bounds.center;

        Quaternion rot = new Quaternion
        {
            eulerAngles = rotation
        };

        for (var i = 0; i < vertices.Length; ++i)
        {
            vertices[i] = rot * (vertices[i] - center) + center;
        }

        mesh.vertices = vertices;
        mesh.RecalculateBounds();
    }

    void Update()
    {

    }
}
