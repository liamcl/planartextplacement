﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARRaycastManager))]
public class PlacementController : MonoBehaviour
{
    [SerializeField]
    private GameObject placedPrefab;

    private List<GameObject> addedInstances = new List<GameObject>();

    private Vector2 touchPosition = default;

    private ARRaycastManager arRaycastManager;

    void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                touchPosition = touch.position;

                if (arRaycastManager.Raycast(touchPosition, hits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon))
                {
                    var hitPose = hits[0].pose;
                    if (addedInstances.Count < 1)
                    {
                        GameObject instance = Instantiate(placedPrefab, hitPose.position, hitPose.rotation);
                        addedInstances.Add(instance);
                    }
                }
            }

            if (touch.phase == TouchPhase.Moved)
            {
                touchPosition = touch.position;

                if (arRaycastManager.Raycast(touchPosition, hits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon))
                {
                    Pose hitPose = hits[0].pose;
                    if (addedInstances.Count > 0)
                    {
                        GameObject instance = addedInstances[addedInstances.Count - 1];
                        instance.transform.position = hitPose.position;
                        instance.transform.rotation = hitPose.rotation;
                    }
                }
            }
        }
    }

    private static List<ARRaycastHit> hits = new List<ARRaycastHit>();
}
